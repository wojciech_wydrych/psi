import java.util.Random;

public class Perceptron {

    double [] weights;
    double threshold;
    double [][] wagi;
    int n2;

    public void Train(double [][] inputs, int [] outputs, double threshold,double lrate, int iterations, int n2)
    {
        this.threshold = threshold;
        int n = inputs[0].length;
        int p = outputs.length;
        this.n2=n2;
        weights = new double[n2];
       wagi=new double[n2][inputs[0].length];
        Random r = new Random();


// INICJALIZACJA WAG
        for(int i=0;i<n2;i++){
            weights[i]=r.nextDouble();
        }
        for(int i=0;i<n2;i++){

            for (int j=0;j<inputs[0].length;j++)
            {
                wagi[i][j]=r.nextDouble();
            }
        }

        for(int i=0;i<iterations;i++){
            int totalError = 0;
            for(int j=0;j<p;j++){
                double output = Output(inputs[j]);
                double error = outputs[j] - output;

                totalError+=error;
                for(int k=0;k<n;k++){
                    //double delta = lrate * inputs[j][k] * error;
                    double delta = output*(1-output)*error;
                    //double delta = lrate * error;
                    double[] delta2=new double[n2];
                    for (int c=0;c<n2;c++)
                    {
                        delta2[c]=Interior_Out(inputs[c])[0]*(1-Interior_Out(inputs[c])[0])*delta*weights[c];
                    }
                    for (int c=0;c<n2;c++)
                    {
                        for (int q=0;q<inputs[0].length;q++)
                        {

                            wagi[c][q]+=delta2[c]*(-1)*lrate;
                        }
                    }
                    weights[k]+=lrate*delta*inputs[j][k];
                }
            }
           if(totalError == 0) break;
        }
    }

    public double[] Interior_Out (double [] input)
    {
       // int n2=2;
        double sum=0.0;
        double[] Out=new double[n2];
        for (int i=0; i<n2; i++) Out[i]=0;

        for (int i=0;i<input.length;i++)
        {
            for(int j=0; j<n2;j++)
            {
                Out[j]+=input[i]*wagi[j][i];

            }
        }

        for (int i=0;i<n2;i++)
        {


            Out[i]=1/(1+Math.pow(Math.E,-Out[i]));
        }

        return Out;
    }

    public double Output(double [] input){
        double sum = 0.0;
        input=Interior_Out(input);
        for(int i=0;i<input.length;i++){
            sum+=weights[i]*input[i];

        }
//System.out.println(sum);
        double end;
       //end=sum;
        end=1/(1+Math.pow(Math.E,-sum));
       // System.out.println("KONIEC "+end);
        //if(end>=threshold) return 1;
        //else return 0;
 return end;

    }
}
