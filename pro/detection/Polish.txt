Adam Bernard Mickiewicz urodzi� si� 24 XII 1798 r. by�o to w wigilie BoSego
narodzenia w Zosiu ko�o Nowogr�dka� Jego Rodzice nie wr�Syli mu zbyt kolorowej
przysz�o�ci gdyS m�ody poeta urodzi� si� w roku �mierci ostatniego kr�la polski�(2)

Do roku 1806 m�ody Adam wychowywa� si� na dw�ch gospodarstwach, jednym miejskim
drugim wiejskim. Pierwsz� szko��, w kt�rej Adam rozpocz�� edukacj� by�a szko�a
Dominikan�w w Nowogr�dku, gdzie ucz�szcza� ze swoim starszym o 2 lata bratem
Franciszkiem. Rodzice Adama tj. Miko�aj Mickiewicz i Barbara z domu Majewska byli
lud�mi dobrze wykszta�conymi, Ojciec by� adwokatem w Nowogr�dku. Miko�aj jako
wykszta�cony cz�owiek rozwija� w swoich dzieciach wyobra�ni� opowiadaj�c im najr�Sniejsze
bajki i ba�nie. Adam by� drugim spo�r�d pi�ciu syn�w Mickiewicz�w. Mia�
starszego brata Franciszka i m�odszych: Aleksandra, Jerzego i Antoniego, kt�ry zmar�
w dzieci�stwie. Wychowywa� si� w trudnych warunkach materialnych. Ojciec poety
sprawowa� urz�d komornika miejskiego i mia� praktyk� adwokack� w Nowogr�dku,
mimo to jego dochody by�y niskie. Drug� szko�� w karierze Poety by�a szko�a powiatowa.
W szkole tej Mickiewicz uczy� si� dobrze, lecz jego wyniki w nauce nie by�y zbyt
zadowalaj�ce. Powtarza� trzeci� klas� najprawdopodobniej z powodu s�abego zdrowia,
gdyS nie ucz�szcza� do szko�y. Zar�wno Adam jak i jego koledzy nie wspominali mi�o
ani nauki, ani nauczycieli. Milej zapami�tali zabawy w wojsko, zorganizowane w bataliony
pod patronatem wychowawc�w. Jako m�ody ch�opak grywa� r�wnieS w teatrze,
jednak ze wzgl�du na jego drobn� budow� przypada�y mu role kobiece, gra� nawet tytu-
�ow� rol� w �wieSo napisanej przez Feli�skiego �Barbarze Radziwi���wnie�. Maj�c
jedena�cie lat zacz�� pisa� wiersze, co naleSa�o wtedy do programu nauczania. Jego
prace nie wyr�Snia�y si� niczym szczeg�lnym od prac innych koleg�w, takSe nic nie
wskazywa�o na to by m�ody Adam mia� zosta� w przysz�o�ci wspania�ym poet� i pisarzem.

II.1 Po wojnie
Po wybuchu wojny i wkroczeniu armii Napoleona na tereny Litwy Barbara Mickiewicz
wraz ze swymi dzie�mi przenios�a si� do drewnianej oficyny. Adam od wrze-
�nia 1813 roku przebywa� pewien czas w pobliskiej Rucie u Medarda Rostockiego,
gdzie jako pi�tnastolatek kocha� si� w poznanej tam J�zi czy Joasi. 
Adam Mickiewicz � Etapy z Sycia
Maksymilian Wi�niewski 10
 Po uko�czeniu nauki w nowogr�dzkiej szkole w 1815 r., razem ze swym przyjacielem,
Janem Czeczotem, zg�osi� si� do Wile�skiego Instytutu Medycznego, lecz
z nieznanych przyczyn nie zosta� przyj�ty. Postanowi�, wi�c zdawa� na Uniwersytet
Wile�ski, kt�ry na pocz�tku XIX w. by� wielkim o�rodkiem intelektualnym kszta�c�-
cym m�odzieS w duchu o�wiecenia. Na czterech wydzia�ach: fizyko-matematycznym,
nauk moralnych, lekarskim i filologicznym studiowa�o 600 student�w! A wszystko
to dzi�ki ksi�ciu Adamowi Czartoryskiemu, kt�ry do 1824r. by� jego kuratorem. Zale-
Sa�o mu, bowiem na tym, aby uniwersytet utrzyma� niezaleSno�� i wysoki poziom nauczania.
PowaSna rusyfikacja rozpocz�a si�, dopiero w�wczas, gdy Czartoryskiego na
tym stanowisku zast�pi� Miko�aj Nowosilcow, kt�ry nienawidzi� Polak�w. Mickiewicz
rozpocz�� studia na wydziale fizyko-matematycznym, a dopiero na drugim roku m�g�
zmieni� kierunek na wydzia� nauk filologicznych, poniewaS taki system obowi�zywa�
wszystkich student�w, kt�rzy otrzymali stypendia nauczycielskie. KaSdy rok studi�w
musieli p�niej odpracowywa� przez dwa lata w szkole �redniej. 
Adam Mickiewicz � Etapy z Sycia
Maksymilian Wi�niewski 11
Rozdzia� IV
Cierpienia, poraSki, przykro�ci
Jednym z pierwszych wydarze�, kt�re wstrz�sn�y m�odziutkim Adamem by�
poSar Nowogr�dka, kt�ry poch�on�� prawie po�ow� miasteczka. Natomiast maj�c 12 lat
juS poczu� okropno�ci Sycia gdyS zmar� jego m�odszy brat, Anto�. 16 Marca 1812 r.
Zmar� Ojciec Adama Miko�aj Mickiewicz, juS wtedy czternastoletni Adam musia� boryka�
si� z problemami finansowymi. I tak od najm�odszych lat Sycie przed wspania�ym
poet� okresu romantyzmu w Polsce Sycie nie by�o �us�ane r�Sami�
 W 1812 roku bardzo ci�Sko zachorowa�, lecz jak w p�niejszym czasie opisywa�
to wydarzenie, twierdzi�, Se uratowa�a go Matka Boska, gdyS jego mama mocno si�
do niej modli�a.
 W roku 1817 wsp�lnie z najbliSszymi przyjaci�mi - Tomaszem Zanem, Janem
Czeczotem, J�zefem JeSowskim, Onufrym Pietraszkiewiczem, Franciszkiem Malewskim
- Mickiewicz za�oSy� tajne Towarzystwo Filomat�w (Mi�o�nik�w Nauki), a nast�pnie
Towarzystwo Filaret�w (Mi�o�nik�w Cnoty). Stowarzyszenia te, prowadzi�o
zr�Snicowane formy pracy samokszta�ceniowej, mia�y na celu przygotowanie m�odzie-
Sy do dzia�alno�ci w r�Snych dziedzinach Sycia narodu pozbawionego w�asnej pa�-
stwowo�ci. Pierwszym znacz�cym do�wiadczeniem sta� si�, wi�c dla Mickiewicza
udzia� w m�odzieSowym "zwi�zku bratnim", w kt�rym zdobywa� umiej�tno�� kierowania
tajn� organizacj�, pisania dokument�w programowych oraz pos�ugiwania si� literatur�
i towarzysk� zabaw� w celach patriotyczno-wychowawczych. Literackim wyrazem
tej atmosfery i do�wiadcze� sta�a si� m. in. Pie�� filaret�w (1820), a najog�lniejszym
programem ideowym. W przysz�o�ci przyczyni� si� do zes�ania Mickiewicza w g��b
Rosji za spraw� wykrycia w roku 1823 organizacji filomackich. W�adze carskie osadzi-
�y Mickiewicza w zamienionym na wi�zienie klasztorze bazylian�w w Wilnie. (Do dzi�
zachowa�a si� cela, w kt�rej siedzia�, nazywana cel� Konrada). Po procesie skazano go
jesieni� 1824 r. wraz z kolegami na osiedlenie si� w g��bi Rosji. Wi�zienie, proces
i zes�anie - to kolejne etapy Syciowych do�wiadcze� i charakterystyczne rysy romantycznej
biografii poety. 
Adam Mickiewicz � Etapy z Sycia
Maksymilian Wi�niewski 12
Rozdzia� V
Na emigracji
G��wn� przyczyn� rozpoczynaj�c�, emigracj� by�o zes�anie w g��b Rosji
za dzia�alno�� w zwi�zkach Filomat�w i Filaret�w. W 1924 r. opu�ci� Litw� z rozkazu
Senatora Nowoscilowa i uda� si� na zes�anie w g��b Rosji. Pi�cioletni pobyt Mickiewicza
w Rosji by� z czasem dla niego owocnym. Od razu znalaz� si� w�r�d kulturalnych
elit Moskwy i Petersburga. Mia� powodzenie u kobiet. Wieszcz zawiera� znajomo�ci
z rodakami � znanym orientalist� J�zefem S�kowskim, a takSe przepowiadaj�cym przysz�e
wydarzenia malarzem J�zefem Oleszkiewiczem.
 Miesi�c trwa�a jego podr�S z Odessy do Moskwy gdyS po drodze zatrzyma� si�
w Charkowie. W Moskwie zamieszka� wraz z JeSowskim u Malewskiego. Wygna�cy
Syli z pocz�tku samotnie, musieli by� ostroSni gdyS przybyli do stolicy dawnej Rusi
w chwili, kiedy po krwawym st�umieniu powstania dekabryst�w w Petersburgu zacz�to
masowe aresztowania i zsy�ki. Nikt nie m�g� si� czu� bezpiecznie we w�asnym domu
takiej atmosferze Adam opracowywa� �Sonety� i �Konrada Wallenroda�. I tak rozpocz��
podr�S po Europie, przed Adamem Mickiewiczem otwiera�y si� kolejne wspania�e
miasta: Berlin, Praga, Florencja, Rzym, Neapol, Genewa oraz ParyS. 5 Czerwca 1829
Mickiewicz dotar� do Berlina. Gdzie po kr�tkim pobycie zapozna� przebywaj�cych tam
Polak�w.
 �Sta�em si� troch� sentymentalny i � nawet pisze dziennik podr�Sy, ale na ten
raz nie udziel� ci wyj�tk�w, bo niepewny jestem czy list dojdzie�(11)- pisa� Mickiewicz
do Malewskiego swojego przyjaciela z Hamburga, do kt�rego przyby� po barwnej przyg�d
Segludze przez Travemunde i Lubek�. Niestety dziennik ten nie zosta� zachowany
i dzi� moSemy tylko przypuszcza� jak wygl�da�a trasa Seglugi. W Hamburgu nie zabawi�
zbyt d�ugo gdyS przebywa� tam tylko kilka dni. Oko�o 11 wyjeSdSaj�c z Berlina
Mickiewicz udaje si� do Drezna sk�d ma zamiar wyruszy� do Szwajcarii oraz Pragi.
W Dre�nie zn�w pisze listy do Malewskiego, w kt�rych oznajmia, Se � Drezno ciasne
i ciemne�.
Mickiewicz uwaSa� sw�j pobyt w Pradze za Melancholijny, bardzo podoba�y mu
si� d�wi�ki bij�cych zegar�w praskich. I w tak przemi�ej atmosferze uda� si�, do Karlsbadu,
gdzie spotka� Ody�ca. 
Adam Mickiewicz � Etapy z Sycia
Maksymilian Wi�niewski 13
Ze swym przyjacielem postanowi� uda� si� do Weimaru by spotka� tam Joanna Wolfganga
Goethego (patrz roz. Romantyzm wspania�� epok�) wspania�ego pisarza. Wizyta
Mickiewicza w Weimarze nie mog�a w Sadnym wypadku doprowadzi� do zbliSenia obu
poet�w. To, co dzieli�o ich od siebie by�o nie do przekroczenia, zbyt duSa r�Snica wieku
oraz s�awa Goethego. Lecz wizyta ta jak kaSda da�a wiele Mickiewiczowi, pozna� kolejnego
wielkiego poet�. Dwudziestego pi�tego wrze�nia o brzasku Mickiewicz i Odyniec
zacz�li swoj� g�rsk� w�dr�wk� ku W�ochom, w�r�d mg�y i deszczu. Drog� pokonywali
na jednym koniem, cz�sto pieszo. Stan�li u podn�Sa wysokiej ska�y Realty, kt�-
ra wznosi si� w�r�d r�wnie stromych i dzikich olbrzym�w. Od st�p tej ska�y rzymsk�
ruin� zaczyna si� w�w�z zwany Via Mala. Droga wije si� w�r�d z�om�w skalnych nad
przepa�ci�. Podczas gdy szczyty zakryte s� chmurami, a przepa�� k��bi si� ciemno�ci�.
Gdy przybyli do Andeer podziwiali pi�kn� kaskad� Renu, kt�ra w dolinie ��czy si�
z potokiem Averserbrach. W Andeer nie zabawili d�ugo, gdyS chcieli jak najszybciej
dotrze� do Rzymu. I tak teS si� sta�o 18 listopada 1829 roku. � Dzie� by� pi�kny, zielone
doliny, wzg�rza i gaje przypomina�y podr�Snym krajobrazy Litwy�. Czas miedzy
28 listopada 1829, a lutym 1830 roku w Rzymie Mickiewiczowi p�yn�� wy�mienicie.
Odbywa�y si� bale, uroczysto�ci ko�cielne, przyjeSdSali i odjeSdSali cudzoziemcy. Nowe
znajomo�ci zawi�zywa�y si� tam b�yskawicznie.
 12 grudnia Mickiewicz sk�ada wizyt� kr�lowej Hortensji. Poznaje jej syna,
przysz�ego cesarza Francji. W grudniu dotar�a wiadomo�� o wybuchu powstania
w Warszawie do Adama dzi�ki Henrykowi Rzewuskiemu. �O �wicie zadzwoni� kto�
raz, drugi. Trzeci, gwa�townie. Na wp� rozbudzony ujrza� We drzwiach Henryka Rzewuskiego.
Rzewuski krzykn��: - powstanie w Warszawie! Mickiewicz powiedzia� cicho
tylko jedno s�owo � Nieszcz�cie!
 Pr�ba przy��czenia do powstania. Romantyczna w�dr�wka, stanowi�ca kolejny
charakterystyczny rys biografii poety, zosta�a zako�czona. Mickiewicz zdecydowa�
si� wraca� do ojczyzny z zamiarem przy��czenia si� do powstania. Po bezowocnych,
okrytych tajemnic� oczekiwaniach przyby� w czerwcu 1831 r. do ParySa, a st�d pod
przybranym nazwiskiem (Adam Muhl) zosta� skierowany przez Legacj� Polsk� (przedstawicielstwo
rz�du powsta�czego) do Warszawy. W sierpniu dotar� do Wielkopolski.
Wobec jednak wyra�nych oznak zbliSaj�cej si� kl�ski z zamiaru przedostania si� do
Kr�lestwa zrezygnowa�. Gotowo�� wzi�cia udzia�u w walce zbrojnej o niepodleg�o��
oraz zwi�zane z tym przeSycia - to dalszy znamienny rys biografii romantycznego poety.

Adam Mickiewicz � Etapy z Sycia
Maksymilian Wi�niewski 14
 W Wielkopolsce pozosta� do wiosny 1832 r., bawi�c w ziemia�skich dworach,
m. in. Sk�rzewskich w Kopaszewie, G�rze�skich w �mie�owie (obecnie muzeum poety),
Turn�w w Objezierzu i Grabowskich w �ukowle. Tutaj po raz pierwszy "nawdycha�
si� polszczyzny" i zebra� mn�stwo obserwacji, kt�re spoSytkowa� nast�pnie w Panu
Tadeuszu. Wed�ug relacji pami�tnikarskiej wygl�da� wtedy nast�puj�co: "brunet,
oczy ciemnozielone, w�osy czarne, nos pi�kny, w ustach cz�sty grymas, a Se nosi
po parysku brod�, wi�c si� wydaje jak uczony �yd. Fizjonomia nic nie pokazuje, cz�sto
zamy�lony, a rzadko weso�y, liberalista wielki, g��boko uczony". (A. Turno, zapis
w pami�tniku pod dat� 25 grudnia 1831
WyjeSdSaj�c z go�cinnej Wielkopolski do Drezna, poeta podzieli� losy wielu
rozbitk�w powstania i rozpocz�� kolejny, typowy rozdzia� swojej biografii przymusow�
wieloletni� emigracj�. 31 lipca 1832 r. przyby� z Drezna do ParySa, gdzie z niewielkimi
przerwami sp�dzi� reszt� Sycia. Wkr�tce po przyje�dzie nawi�za� bliskie stosunki
z francuskim �rodowiskiem artystycznym i intelektualnym (m. in. rze�biarzem Dawidem
d�Angers, poet� Hugues Felicite Lamennais, historykiem Charlesem de Montalambertem.
W��czy� si� takSe aktywnie w Sycie emigracji (cz�onkostwo w Towarzystwie
Literackim, Towarzystwie Litewskim i Ziem Ruskich, Towarzystwie Pomocy Naukowej,
Komitecie Narodowym Lelewela i in.), chc�c przygotowa� j� do powrotu do kraju.
Przewiduj�c rych�y wybuch og�lnoeuropejskiej rewolucji, s�dzi�, podobnie jak ca�a
emigracja, Se pobyt na obczy�nie nie potrwa d�ugo. G��wnymi, wi�c utworami zwi�zanymi
z t� dzia�alno�ci� poety sta�y si� "Ksi�gi narodu polskiego" i "Ksi�gi pielgrzymstwa
polskiego" (1832) oraz artyku�y og�aszane w "Pielgrzymie Polskim", kt�rego przez
pewien okres by� takSe g��wnym redaktorem (kwiecie� - czerwiec 1833). Zm�czony
emigracyjnym Syciem oraz rozczarowany wobec monarchistycznej Europy, poddawany
wraz z ca�� emigracj� r�Snym zakazom i szykanom (m. in. zakaz przenoszenia si�
z miejsca na miejsce), wskrzesza� warto�� narodowej historii, tworz�c "Pana Tadeusza"
(1833, wyd. 1834), lub zag��bia� si� w lektury dzie� mistycznych, czego owocem sta� si�
m. in. cykl uniwersalnych aforyzm�w zatytu�owany "Zdania i uwagi" (1833 - 1834). 